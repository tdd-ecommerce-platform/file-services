package com.tdd.fileservices.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
@RequestMapping("/file-services")
public class FileController {

    @PostMapping
    public String multiUpload(@ModelAttribute("files") MultipartFile[] files) throws IOException {
        for (MultipartFile file : files) {
            Files.write(Paths.get("D:\\Tiktok video\\douyin42\\" + file.getOriginalFilename()), file.getBytes());
        }
        return "redirect:/";
    }
}
